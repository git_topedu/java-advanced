package DesignPatterns.Singleton;

/*
 * Problem
 * What about Object creation using Clone and Serialization?? Next approach will handle that problem.
 */

class JBT5 {
    /*
     * As private constructor is used so can not create object of this class
     * directly. Except by using static method of same class.
     */
    private JBT5() {
    }

    /*
     * Here static inner class is used instead of Static variable. It means Object
     * will be lazy initialized.
     */
    private static class LazyInit {
        private static final JBT5 instance = new JBT5();
    }

    /*
     * Whenever object JBT is required this method will be invoked and it will
     * return the instance of JBT.
     */
    static JBT5 createInstance() {
        return LazyInit.instance;
    }

    int i;
}