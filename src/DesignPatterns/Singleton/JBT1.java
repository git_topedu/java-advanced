package DesignPatterns.Singleton;

/*
 * Here we will create a private Constructor and also a static method to create an object of the same class.
 */

/*
 * Problem
 * What will happen if 2 different thread enters the createInstance method at the same time when the instance is null.
 * In that case, threads will create 2 different objects of JBT. Which is against the Singleton pattern. In the next approach,
 * this problem can be solved.
 */

class JBT1 {
    /*
     * This variable will be used to hold reference of JBT class.
     */
    private static JBT1 instance = null;
	/*
	 * As private constructor is used so can not create object of this class
	 * directly. Except by using static method of same class.
	 */
	private JBT1() {
	}

    /*
     * This method will be used to get instance of JBT class. This method will check
     * if there is aready an object of class create or not if not then it will
     * create an Obect of JBT class and return the same else it will return the
     * existing Object.
     */
    static JBT1 createInstance() {
        if (instance == null) {
            instance = new JBT1();
            return instance;
        } else
            return instance;
    }

    int i;
}