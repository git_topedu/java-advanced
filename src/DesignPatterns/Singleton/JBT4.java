package DesignPatterns.Singleton;

/*
 * Problem
 * All problem has been solved in this approach still synchronized keyword is used(Once) and that should be avoided.
 */

class JBT4 {
    /*
     * This variable will be used to hold reference of JBT class.
     * 
     * Here we are creating the instance of JBT class and assigning the reference of
     * that object to instance.
     */
    private static JBT4 instance = null;

    /*
     * As private constructor is used so can not create object of this class
     * directly. Except by using static method of same class.
     */
    private JBT4() {
    }

    /*
     * This method will be used to get instance of JBT class. This method will check
     * if there is already an object of class create or not if not then it will
     * create an Object of JBT class and return the same else it will return the
     * existing Object.
     * 
     * Now block is marked as synchronized instead of whole method. So synchronized
     * part will be used only once and that is when object is null.
     */
    static JBT4 createInstance() {
        if (instance == null) {
            synchronized (JBT4.class) {
                if (instance == null) {
                    instance = new JBT4();
                    return instance;
                }
            }
        }
        return instance;
    }

    int i;
}