package DesignPatterns.Singleton;

public class SingletonDemo {
    public static void main(String[] args) {

        // illegal construct
        // Compile Time Error: The constructor JBT6() is not visible
        // JBT6 object = new JBT6();

        // Get the only object available
        JBT6 object = JBT6.createInstance();

        // show the message
        System.out.println(object.readResolve());
        System.out.println(object.readResolve());

    }
}