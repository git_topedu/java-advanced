package DesignPatterns.Singleton;

/*
 * Problem
 * Here we are creating the object of JBT when class gets loaded. The object gets created even when it is not required.
 * The object should be created only when it is required(Lazy Loading).
 * In the next approach, we will try to resolve this problem by using Double checking locking.
 */

class JBT3 {
    /*
     * This variable will be used to hold reference of JBT class.
     * 
     * Here we are creating the instance of JBT class and assigning the reference of
     * that object to instance.
     */
    private static JBT3 instance = new JBT3();

    /*
     * As private constructor is used so can not create object of this class
     * directly. Except by using static method of same class.
     */
    private JBT3() {
    }

    /*
     * This method will be used to get instance of JBT class. This method will check
     * if there is already an object of class create or not if not then it will
     * create an Object of JBT class and return the same else it will return the
     * existing Object.
     * 
     * synchronized keyword is not required here.
     */
    static JBT3 createInstance() {
        /*
         * As instance is already create and class loading time hence we can directly
         * return the same without creating any object.
         */
        return instance;
    }

    int i;
}