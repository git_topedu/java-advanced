package DesignPatterns.Singleton;

import java.io.Serializable;

/*
 * To stop cloning of Object we will implement the Cloneable interface and throw CloneNotSupportedException.
 * Also Serialized interface will be implemented and readObject will be used to return only one object at all time.
 */

class JBT6 implements Cloneable, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new CloneNotSupportedException();
    }

    protected Object readResolve() {
        return createInstance();
    }

    /*
     * As private constructor is used so can not create object of this class
     * directly. Except by using static method of same class.
     */
    private JBT6() {
    }

    /*
     * Here static inner class is used instead of Static variable. It means Object
     * will be lazy initialized.
     */
    private static class LazyInit {
        private static final JBT6 instance = new JBT6();
    }

    /*
     * Whenever object JBT is required this method will be invoked and it will
     * return the instance of JBT.
     */
    static JBT6 createInstance() {
        return LazyInit.instance;
    }

    @Override
    public String toString() {
        return "Instance sn: " + serialVersionUID;
    }

    int i;
}