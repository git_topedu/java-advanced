package DesignPatterns.Singleton;

/*
 * In this approach, we will make createInstance method synchronized so only one thread is allowed in that class and only one
 * object will be created instead of Two.
 */

/*
 *
 * Problem
 * The moment we use synchronized keyword it will create a problem for our multi-threaded application in terms of performance.
 * So on one side, we are resolving the problem on another side we are creating one more problem.
 * In the next approach, we will solve both this problem.
 */

class JBT2 {
    /*
     * This variable will be used to hold reference of JBT class.
     */
    private static JBT2 instance = null;

    /*
     * As private constructor is used so can not create object of this class
     * directly. Except by using static method of same class.
     */
    private JBT2() {
    }

    /*
     * This method will be used to get instance of JBT class. This method will check
     * if there is aready an object of class create or not if not then it will
     * create an Obect of JBT class and return the same else it will return the
     * existing Object.
     * 
     * Now method is marked as synchronized hence only one threa will be allowed to
     * enter in this method hence only one object will be created.
     */
    static synchronized JBT2 createInstance() {
        if (instance == null) {
            instance = new JBT2();
            return instance;
        } else
            return instance;
    }

    int i;
}