package streams;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class WriterDemo {
    public static void main(String args[]) throws IOException {

        File file = new File("rose.txt");
        // file.createNewFile();
        FileWriter txt = new FileWriter(file);
        PrintWriter out = new PrintWriter(txt);

        out.append("Welcome to roseindia.net\n");
        char ch[] = { 'n', 'e', 'w', 's', 't', 'r', 'a', 'c', 'k', 'i', 'n', 'd', 'i', 'a', '.', 'c', 'o', 'm' };
        out.print("\n\tWelcome to ");
        out.write(ch);

        out.close();

    }
}