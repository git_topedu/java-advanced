package streams;

import java.io.*;

public class FileInStreamDemo {

    public static void main(String[] args) {

        // create file object
        File file = new File("DevFile.txt");

        int ch;
        StringBuffer strContent = new StringBuffer("");
        FileInputStream fin = null;

        try {
            fin = new FileInputStream(file);

            while ((ch = fin.read()) != -1)
                strContent.append((char) ch);

            fin.close();

        } catch (FileNotFoundException e) {
            System.out.println("File " + file.getAbsolutePath() + " could not be found on filesystem");
        } catch (IOException ioe) {
            System.out.println("Exception while reading the file" + ioe);
        }

        System.out.println("File contents :");
        System.out.println(strContent);

    }
}