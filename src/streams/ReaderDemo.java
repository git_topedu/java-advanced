package streams;

import java.io.Reader;
import java.io.FileReader;
import java.io.IOException;

public class ReaderDemo {
    public static void main(String args[]) {
        Reader rdr = null;

        try {
            rdr = new FileReader("file.txt");
            int c;
            System.out.println();
            System.out.println("***** OUTPUT *****");
            System.out.println();
            while ((c = rdr.read()) != -1) {
                System.out.print((char) c);
            }
            System.out.println();
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            if (rdr != null) {
                try {
                    rdr.close();
                } catch (IOException ioe) {
                    System.out.println(ioe);
                }
            }
        }
    }
}