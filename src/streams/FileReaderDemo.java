package streams;

import java.io.FileReader;
import java.io.IOException;

public class FileReaderDemo {
    public static void main(String args[]) {
        FileReader fr = null;

        try {
            fr = new FileReader("file.txt");
            int c;
            System.out.println();
            System.out.println("***** OUTPUT *****");
            System.out.println();
            while ((c = fr.read()) != -1) {
                System.out.print((char) c);
            }
            System.out.println();
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ioe) {
                    System.out.println(ioe);
                }
            }
        }
    }
}