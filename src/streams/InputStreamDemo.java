package streams;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class InputStreamDemo {
    public static void main(String args[]) {
        InputStream is = null;
        try {
            is = new FileInputStream("read.txt");
            int c;
            System.out.println();
            System.out.println("reading text.....");
            while ((c = is.read()) != -1) {
                System.out.println((char) c);
            }
            System.out.println();
        } catch (IOException ioe) {
            System.out.println(ioe);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    System.out.print(e);
                }
            }
        }
    }
}