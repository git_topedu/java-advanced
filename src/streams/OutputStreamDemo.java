package streams;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class OutputStreamDemo {
    public static void main(String args[]) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream("abc.txt");
            fos = new FileOutputStream("xyz.txt");
            int r;
            System.out.println();
            System.out.println("******* Contents of abc.txt file has been written into the xyz.txt ***********");
            System.out.println();
            while ((r = fis.read()) != -1) {
                fos.write(r);
            }
            System.out.println();
        } catch (IOException e) {
            System.out.println("IOException caught..!!");
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException ioe) {
                    System.out.println(ioe);
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException ie) {
                        System.out.println(ie);
                    }
                }
            }
        }
    }
}