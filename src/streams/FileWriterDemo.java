package streams;

import java.io.File;
import java.io.FileWriter;

class FileWriterDemo {
    public static void main(String args[]) {
        String str = "This example demonstrates you how to write to file using FileWriter";
        File file = null;
        FileWriter fw = null;
        try {
            file = new File("writeTofileFileWriter.txt");
            fw = new FileWriter(file);
            fw.write(str);
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("File is created and contents are written into successfully");
    }
}