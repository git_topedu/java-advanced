package exceptions;

public class Program {
    public static void main(String[] args) {
        // if we will execute the code, the program will be terminate due to unhandled exception.  
        // System.out.println(divideArray(args));

        try {
            System.out.println(divideArray(args));
        } catch (Exception e) {
            System.out.println("The program will not be terminated");
        }
    }

    private static int divideArray(String[] array) {
        String s1 = array[0];
        String s2 = array[1];
        return divideStrings(s1, s2);
    }

    private static int divideStrings(String s1, String s2) {
        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);
        return divideInts(i1, i2);
    }

    private static int divideInts(int i1, int i2) {
        return i1 / i2;
    }
}